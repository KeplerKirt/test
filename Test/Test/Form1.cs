﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using FastTreeNS;


namespace Test
{
    public partial class Form1 : Form
    {
        DateTime date1 = new DateTime(0, 0);

        private static ManualResetEvent mre = new ManualResetEvent(false);

        string StrPath;
        public Form1()
        {
            InitializeComponent();

        }
        private int found;
        private string Derekt;
        private string Time1;
        

        private void BuildTree(Node root, string fileMask)
        {
            var toProcess = new Stack<Node>();
            toProcess.Push(root);

            while (toProcess.Count > 0)
            {
                var node = toProcess.Pop();
                try
                {
                    try
                    {

                   
                    foreach (var dir in Directory.GetDirectories(node.FullPath))
                    {
                        var n = new Node { FullPath = dir };
                        node.Add(n);
                        toProcess.Push(n);
                        Derekt = n.Name;
                    }

                    foreach (var file in Directory.GetFiles(node.FullPath, fileMask)) ///оп оп
                    {
                        var n = new Node { FullPath = file, IsFile = true };
                        node.Add(n);
                        found++;
                    }
                    }
                    catch { }
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
            timer1.Enabled = false;
        }
        public static void SleepAndSet()
        {
            Thread.Sleep(2000);
            mre.Set();
        }

        private void Poisk1()
        {

            var dir = StrPath;

            //корневая директория
            var root = new Node { FullPath = dir };


            var thread = new Thread(() => BuildTree(root, "*" + TB_Shaplon.Text + "*")) { IsBackground = true };
            thread.Start();

            var ft = new FastTree { Parent = this.panel1, Dock = DockStyle.Fill, ShowIcons = true, ShowRootNode = true };
            ft.BringToFront();
            //ft.ShowRootNode = true;
            //ft.ShowIcons = true;

            Application.Idle += delegate
            {
                if (thread.IsAlive || !thread.IsAlive)
                {
                    ft.Build(root);
                    Text = "Found files: " + found + "  Прошло времени-" + Time1 + "   Обработка директории: " + Derekt;
                }
            };
            
        }

        private void Poisk2()
        {
            var dir = StrPath;

            //корневая директория
            var root = new Node { FullPath = dir };

            var thread = new Thread(() => BuildTree1(root, "*"+TB_Shaplon.Text + "*"+"*.txt*")) { IsBackground = true };
            thread.Start();

            var ft = new FastTree() { Parent = this.panel1, Dock = DockStyle.Fill, ShowIcons = true, ShowRootNode = true};
            ft.BringToFront();

            Application.Idle += delegate
            {
                if (thread.IsAlive || !thread.IsAlive)
                {
                    ft.Build(root);
                    Text = "Found files: " + found + "  Прошло времени-" + Time1 + "   Обработка директории: " + Derekt;
                }
            };
        }


        //для поиска по тексту
        private void BuildTree1(Node root, string fileMask)
        {
            var toProcess = new Stack<Node>();
            toProcess.Push(root);
            while (toProcess.Count > 0)
            {
                var node = toProcess.Pop();
                try
                {
                    try
                    {

                    
                    foreach (var dir in Directory.GetDirectories(node.FullPath))
                    {
                        var t = new Label();
                        var n = new Node { FullPath = dir };
                        node.Add(n);
                        toProcess.Push(n);
                        Derekt = n.Name;
                    }
                    
                    foreach (var file in Directory.GetFiles(node.FullPath, fileMask)) ///оп оп
                    {
                        try
                        {
                            StreamReader sr = new System.IO.StreamReader(file, Encoding.Default);
                            string s = sr.ReadToEnd();
                            sr.Close();
                        
                        var n = new Node { FullPath = file, IsFile = true };
                        if (s.Contains(TB_pois_in.Text))
                        {
                            node.Add(n);
                            found++;
                        }
                        }
                        catch
                        {
                        }
                    }
                    }
                    catch { }
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
            timer1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text1.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_Shaplon.Text);
                SW.Close();
            }
            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text2.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_pois_in.Text);
                SW.Close();
            }
            found = 0;
            Poisk1();
            //таймер
            date1 = new DateTime(0, 0);
            timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StrPath = FBD.SelectedPath;
                LB_string.Text = StrPath;
                radioButton1.Visible = true;
                radioButton2.Visible = true;

                using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text3.txt", FileMode.Create, FileAccess.Write)))
                {
                    SW.Write(LB_string.Text);
                    SW.Close();
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            found = 0;
            Poisk2();

            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text1.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_Shaplon.Text);
                SW.Close();
            }
            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text2.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_pois_in.Text);
                SW.Close();
            }
            //таймер
            date1 = new DateTime(0, 0);
            timer1.Enabled = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label1.Visible = true;
            TB_Shaplon.Visible = true;
            button1.Visible = true;

            button3.Visible = false;
            TB_pois_in.Visible = false;
            label2.Visible = false;
        
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label1.Visible = true;
            TB_Shaplon.Visible = true;
            button1.Visible = false;

            button3.Visible = true;
            TB_pois_in.Visible = true;
            label2.Visible = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(File.Exists(Application.StartupPath + @"\" + "Text1.txt"))
            {
                TB_Shaplon.Text = File.ReadAllText(Application.StartupPath + @"\" + "Text1.txt");
                radioButton1.Checked = true;
            }
                

            if (File.Exists(Application.StartupPath + @"\" + "Text2.txt"))
                TB_pois_in.Text = File.ReadAllText(Application.StartupPath + @"\" + "Text2.txt");

            if (File.Exists(Application.StartupPath + @"\" + "Text3.txt"))
            {
                LB_string.Text = File.ReadAllText(Application.StartupPath + @"\" + "Text3.txt");
                StrPath = File.ReadAllText(Application.StartupPath + @"\" + "Text3.txt");
                radioButton1.Visible = true;
                radioButton2.Visible = true;
            }
                
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text1.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_Shaplon.Text);
                SW.Close();
            }
            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text2.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(TB_pois_in.Text);
                SW.Close();
            }

            using (StreamWriter SW = new StreamWriter(new FileStream(Application.StartupPath + @"\" + "Text3.txt", FileMode.Create, FileAccess.Write)))
            {
                SW.Write(LB_string.Text);
                SW.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            date1 = date1.AddSeconds(0.1);
            Time1 = date1.ToString("mm:ss");
        }
    }



    public class Node : IEnumerable<Node>
    {
        private List<Node> nodes;
        //Файл?
        public bool IsFile { get; set; }
        //Полный путь
        public string FullPath { get; set; }
        //Имя
        public string Name { get { return Path.GetFileName(FullPath); } }

        // Имеет хоть один файл у себя или у потомков ?
        public bool HasFile
        {
            get
            {
                return IsFile || Nodes.Any(n => n.HasFile);
            }
        }

        public Node()
        {
            nodes = new List<Node>();
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? FullPath : Name;
        }

        //добавление нода
        public void Add(Node node)
        {
            nodes.Add(node);
        }

        IEnumerable<Node> Nodes
        {
            get
            {
                for (int i = 0; i < nodes.Count; i++)
                    yield return nodes[i];
            }
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return Nodes.Where(n => n.HasFile).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}